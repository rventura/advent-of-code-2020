import numpy as np


def conv_row(s):
    return int("0b" + s.replace("B", "1").replace("F", "0"), 2)


def conv_column(s):
    return int("0b" + s.replace("R", "1").replace("L", "0"), 2)


lines = [l[:-1] for l in open("input/day5.txt").readlines()]
seats = [(s[:7], s[7:]) for s in lines]
row_col = [(conv_row(row), conv_column(col)) for (row, col) in seats]

# Part 1
seat_IDs = np.array([row * 8 + col for (row, col) in row_col])
print("Max seat ID: {}".format(seat_IDs.max()))

# Part 2
sorter = seat_IDs.argsort()
ix_array = (seat_IDs[sorter] - np.arange(seat_IDs.min(), seat_IDs.max())).argmax()
myseat_ID = seat_IDs[sorter][ix_array - 1]
print("My seat ID: {}".format(myseat_ID))
