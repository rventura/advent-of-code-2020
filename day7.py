from collections import defaultdict


def parse_rule(rule):
    content_sep_string = " bags contain "
    content_sep_ix = rule.find(content_sep_string)
    bag_name, content = (
        rule[:content_sep_ix],
        rule[content_sep_ix + len(content_sep_string) :],
    )
    content = content.split(", ")
    if len(content) == 1 and content[0].find("no other bags.") > -1:
        return bag_name, []
    return bag_name, [parse_content(c) for c in content]


def parse_content(c):
    return (int(c[0]), c[2 : c.find(" bag")])


def push_content(num_bags, bag_name, all_rules, total_content, push_intermediate=True):
    bag_content = all_rules[bag_name]
    if bag_content:
        for inner_num, inner_name in bag_content:
            push_content(
                num_bags * inner_num,
                inner_name,
                all_rules,
                total_content,
                push_intermediate,
            )
    if not bag_content or push_intermediate:
        total_content.extend([(num_bags, bag_name)])


def tidy_content(messy_content):
    bag_dict = defaultdict(int)
    for content in messy_content:
        num, name = content
        bag_dict[name] += num
    return bag_dict


def get_containers(all_rules, container_dic):
    new_container_colors = []
    for cont_color, checked in container_dic.items():
        if not checked:
            for k, v in all_rules.items():
                if cont_color in [inner_color for (inner_num, inner_color) in v]:
                    new_container_colors.append(k)
        container_dic[cont_color] = True
    for new_color in set(new_container_colors):
        container_dic.setdefault(new_color, False)


lines = [l[:-1] for l in open("./input/day7.txt").readlines()]
bag_rules = dict(parse_rule(rule) for rule in lines)

# First iteration
containers = {
    col: False
    for col, v in bag_rules.items()
    if "shiny gold" in [inner_color for (inner_num, inner_color) in v]
}

while not all(containers.values()):
    get_containers(bag_rules, containers)

# Part 2
shiny_content = []
push_content(1, "shiny gold", bag_rules, shiny_content)
shiny_content = tidy_content(shiny_content)

print("{} type of bags can contain shiny gold bags".format(len(containers)))
print("A shiny gold bag contains {} bags".format(sum(shiny_content.values()) - 1))
