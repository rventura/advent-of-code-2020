class CupsState:
    def __init__(self, config: list):
        self.state = {}
        for cup, next_cup in zip(config[:-1], config[1:]):
            self.state[cup] = next_cup
        self.state[config[-1]] = config[0]
        self.current_cup_ix = config[0]
        self.picked_up = ()

    def pickup(self):
        v0 = self.state.pop(self.current_cup_ix)
        v1 = self.state.pop(v0)
        v2 = self.state.pop(v1)
        to_glue = self.state.pop(v2)

        self.state[self.current_cup_ix] = to_glue
        self.picked_up = (v0, v1, v2)

    def destination_cup_ix(self):
        new_ix = self.current_cup_ix - 1
        while True:
            if new_ix in self.picked_up:
                new_ix -= 1
            if new_ix < 1:
                new_ix = max(self.state)
            if new_ix in self.state:
                break
        return new_ix

    def replace(self):
        destination_ix = self.destination_cup_ix()
        to_glue = self.state.pop(destination_ix)
        self.state[destination_ix] = self.picked_up[0]
        self.state[self.picked_up[0]] = self.picked_up[1]
        self.state[self.picked_up[1]] = self.picked_up[2]
        self.state[self.picked_up[2]] = to_glue

    def move(self):
        self.pickup()
        self.replace()
        self.current_cup_ix = self.state[self.current_cup_ix]


# Part 1
initial_config = "614752839"
cup_game = CupsState([int(i) for i in initial_config])
for _ in range(100):
    cup_game.move()
labels = []

final_state = cup_game.state.copy()
v = final_state.pop(1)
for _ in range(8):
    labels.append(v)
    v = final_state.pop(v)
print("Labels after cup 1: {}".format("".join(str(i) for i in labels)))

# Part 2
cup_game2 = CupsState([int(i) for i in initial_config] + list(range(10, 1000001)))
for _ in range(10 * 1000000):
    cup_game2.move()

final_state2 = cup_game2.state.copy()
v = final_state2.pop(1)
labels2 = [v, final_state2.pop(v)]
print(
    "Product of the two clockwise-to-1 cups labels: {}".format(labels2[0] * labels2[1])
)
