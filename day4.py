# coding: utf-8
import re
import string

passports = []


def check_values(passport):
    hcl_re = re.compile("#[a-f0-9]{6}")
    hgt_re = re.compile("[0-9]{3}cm|[0-9]{2}in")
    valid_ecl = {"amb", "blu", "brn", "gry", "grn", "hzl", "oth"}
    valid_hgt = {"cm": (150, 193), "in": (59, 76)}

    if not (
        1920 <= int(passport["byr"]) <= 2002
        and 2010 <= int(passport["iyr"]) <= 2020
        and 2020 <= int(passport["eyr"]) <= 2030
        and passport["ecl"] in valid_ecl
    ):
        return False
    if not (
        len(passport["pid"]) == 9 and all([i in string.digits for i in passport["pid"]])
    ):
        return False
    if not hgt_re.match(passport["hgt"]):
        return False
    if hgt_re.match(passport["hgt"]):
        height = int(passport["hgt"][:-2])
        unit = passport["hgt"][-2:]
        if not valid_hgt[unit][0] <= height <= valid_hgt[unit][1]:
            return False
    if not hcl_re.match(passport["hcl"]):
        return False
    return True


def parse_passport(s):
    keys_vals = s.split()
    return dict([kv.split(":") for kv in keys_vals])


with open("input/day4.txt") as f:
    tmp_passport = []
    for line in f.readlines():
        if line != "\n":
            tmp_passport.append(line[:-1])
        else:
            passports.append(parse_passport(" ".join(tmp_passport)))
            tmp_passport = []
    if tmp_passport:
        passports.append(parse_passport(" ".join(tmp_passport)))

mandatory_keys = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]  # , "cid"]
good_passports_p1 = 0
good_passports_p2 = 0

for p in passports:
    if all([k in p.keys() for k in mandatory_keys]):
        good_passports_p1 += 1
        if check_values(p):
            good_passports_p2 += 1

print("Number of valid passports (part 1): {}".format(good_passports_p1))
print("Number of valid passports (part 2): {}".format(good_passports_p2))
