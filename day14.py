from itertools import product

import numpy as np


def to_bool_array(num: int, min_len=36: int) -> np.array:
    bools = [bool(int(x)) for x in bin(num)[2:]]
    while len(bools) < min_len:
        bools.insert(0, False)
    return np.array(bools)


def apply_bitmask(value: int, r_mask: str) -> int:
    and_mask = int("0b" + r_mask.replace("X", "1"), 2)
    or_mask = int("0b" + r_mask.replace("X", "0"), 2)
    return (value | or_mask) & and_mask


def generate_slots(base_slot: int, r_mask: str) -> list:
    or_mask = int("0b" + r_mask.replace("X", "0"), 2)
    base_slot_bin = to_bool_array(base_slot | or_mask)
    X_ix = np.array([ix for ix, s in enumerate(r_mask) if s == "X"])
    all_slots_bin = np.tile(base_slot_bin, (2 ** X_ix.size, 1))
    for i in range(1, 2 ** X_ix.size):
        indexes_mask = to_bool_array(i, X_ix.size)
        toggler = np.zeros_like(base_slot_bin)
        toggler[X_ix] = indexes_mask
        all_slots_bin[i, toggler] = ~all_slots_bin[i, toggler]
    # Convert back to integers
    all_slots_int = [
        int("0b" + "".join(l), 2) for l in all_slots_bin.astype(int).astype(str)
    ]
    return all_slots_int


lines = [l[:-1].split(" = ") for l in open("input/day14.txt").readlines()]

# Part 1
memory = {}
for line in lines:
    instruction, value = line
    if instruction.find("mask") > -1:
        raw_mask = value
    else:
        slot = int(instruction[4:-1])
        masked_value = apply_bitmask(int(value), raw_mask)
        memory[slot] = masked_value
print("Sum of values in memory (part 1): {}".format(sum(memory.values())))

# Part 1
memory = {}
for line in lines:
    instruction, value = line
    if instruction.find("mask") > -1:
        raw_mask = value
    else:
        mem_slot = int(instruction[4:-1])
        all_slots = generate_slots(mem_slot, raw_mask)
        for slot in all_slots:
            memory[slot] = int(value)
print("Sum of values in memory (part 2): {}".format(sum(memory.values())))
