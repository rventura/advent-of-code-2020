def get_nth_spoken(N, initial_numbers):
    spoken = initial_numbers.pop()
    last_appearance = {num: i for i, num in enumerate(initial_numbers)}
    counter = len(last_appearance)
    while counter < N - 1:
        if spoken not in last_appearance:
            last_appearance[spoken] = counter
            spoken = 0
        else:
            age = counter - last_appearance[spoken]
            last_appearance[spoken] = counter
            spoken = age
        counter += 1
    print("{}th spoken number: {}".format(N, spoken))


get_nth_spoken(2020, [6, 19, 0, 5, 7, 13, 1])
get_nth_spoken(30000000, [6, 19, 0, 5, 7, 13, 1])
