def transform(n=1, subject_number=7):
    n *= subject_number
    return n % 20201227


public_key = {}
loop_size = dict()

with open("input/day25.txt") as f:
    public_key["card"] = int(f.readline()[:-1])
    public_key["door"] = int(f.readline()[:-1])

counter = 1
v = 1
while True:
    v = transform(v)
    if v == public_key["card"]:
        loop_size["card"] = counter
        break
    if v == public_key["door"]:
        loop_size["door"] = counter
        break
    counter += 1

first_found = "card" if "card" in loop_size.keys() else "door"
complement = "door" if "card" in loop_size.keys() else "card"

v = 1
for _ in range(loop_size[first_found]):
    v = transform(v, public_key[complement])

encryption_key = v
print("Encryption key: {}".format(encryption_key))
