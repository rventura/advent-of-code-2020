instructions = [l[:-1].split() for l in open("input/day8.txt").readlines()]


def follow_instruction(instruction, acc, pos):
    operation, num = instruction
    if operation == "nop":
        pos += 1
    elif operation == "jmp":
        pos += int(num)
    elif operation == "acc":
        acc += int(num)
        pos += 1
    return acc, pos


def execute_instruction_set(instr):
    position = 0
    accumulator = 0
    followed_instruction = []

    while True:
        if position in followed_instruction:
            break
        followed_instruction.append(position)
        try:
            accumulator, position = follow_instruction(
                instr[position], accumulator, position
            )
        except IndexError:
            break
    return accumulator, position


def flip_instruction(ix, instr):
    instr[ix][0] = "jmp" if instr[ix][0] == "nop" else "nop"


# Part 1 : infinite loop
infinite_acc, _ = execute_instruction_set(instructions)
print("Accumulator value for infinite loop: {}".format(infinite_acc))

# Part 2 : fixing the infinite loop
for i, ins in enumerate(instructions):
    if not ins[0] == "acc":
        flip_instruction(i, instructions)
        final_accumulator, final_position = execute_instruction_set(instructions)
        if final_position == len(instructions):
            print("Accumulator after program fix: {}".format(final_accumulator))
            break
        flip_instruction(i, instructions)
