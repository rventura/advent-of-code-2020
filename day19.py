def parse_rule(seq):
    if seq.find('"') > -1:
        return [seq[1]]
    return [int(i) for i in seq.split()]


def unfold_rule(rule_index: int, mappings: dict) -> list:
    rule_combinations = []
    for subrule in mappings[rule_index]:
        subrule_combinations = [[]]
        for item in subrule:
            if isinstance(item, str):
                for comb in subrule_combinations:
                    comb.append(item)
            elif isinstance(item, int):
                subrule_combinations = [
                    subcomb + subcontent
                    for subcomb in subrule_combinations
                    for subcontent in unfold_rule(item, mappings)
                ]
            else:
                raise ValueError("Item {} should be a character or an int".format(item))
        rule_combinations.extend(subrule_combinations)
    return rule_combinations


# Part 1
lines = [l[:-1] for l in open("input/day19.txt").readlines()]
rules_mappings = {
    int(line.split(": ")[0]): [parse_rule(s) for s in line.split(": ")[1].split(" | ")]
    for line in lines[:139]
}
messages = lines[140:]
valid_messages = set("".join(s) for s in unfold_rule(0, rules_mappings))
valid_count = sum([m in valid_messages for m in messages])
