# coding: utf-8
from collections import Counter, defaultdict
from functools import reduce

import numpy as np

adapters_joltages = [int(l[:-1]) for l in open("input/day10.txt").readlines()]

# Part 1
adapters_joltages.insert(0, 0)
adapters_joltages.insert(0, max(adapters_joltages) + 3)
adapters_joltages.sort()
jolt_diff = np.diff(adapters_joltages)
diff_counter = Counter(jolt_diff)
print(
    "Number of 1-jolt diff * number of 3-jolts diff: {}".format(
        diff_counter[1] * diff_counter[3]
    )
)

# Part 2

one_series = defaultdict(int)
counter = 0
for el in jolt_diff:
    if el == 1:
        counter += 1
    else:
        one_series[counter] += 1
        counter = 0
ARRANGEMENTS = {2: 2, 3: 4, 4: 7, 5: 13}

valid_config = reduce(
    lambda x, y: x * y, [C ** one_series[k] for k, C in ARRANGEMENTS.items()]
)
print("Number of valid ways to plug the adapters: {}".format(valid_config))
