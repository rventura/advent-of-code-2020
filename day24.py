from collections import defaultdict
from functools import reduce

import numpy as np


def parse_line(symbols):
    instructions = []
    characters = list(symbols)
    while characters:
        symbol = characters.pop(0)
        if symbol in ["n", "s"]:
            complement = characters.pop(0)
            instructions.append("".join([symbol, complement]))
        else:
            instructions.append(symbol)
    return instructions


flip_instructions = [
    parse_line(line[:-1]) for line in open("input/day24.txt").readlines()
]

# Part 1
instructions_to_coord = {
    "e": (2, 0),
    "w": (-2, 0),
    "nw": (-1, 2),
    "ne": (1, 2),
    "sw": (-1, -2),
    "se": (1, -2),
}
tiles = defaultdict(bool)
for instruction in flip_instructions:
    tile_coord = reduce(
        lambda x, y: x + y,
        [np.array(instructions_to_coord[ins]) for ins in instruction],
    )
    tile_ix = tuple(tile_coord.tolist())
    tiles[tile_ix] = not tiles[tile_ix]

print("Black tiles after initial instructions: {}".format(sum(tiles.values())))

# Part 2
class FloorTiling:
    def __init__(self, initial_state: defaultdict):
        """Cellular automaton of hexagonal floor tiles"""
        self.state = initial_state

    def prepare_reachable_tiles(self):
        reachable_ix = set(
            neigh_ix for ix in self.state for neigh_ix in self.tile_neighborhood(ix)
        )
        # for ix in self.state:
        #     for neigh_ix in self.tile_neighborhood(ix):
        #         reachable_ix.add(neigh_ix)
        for reach_ix in reachable_ix:
            self.state.setdefault(reach_ix, False)

    def tile_neighborhood(self, ix: tuple):
        return [(ix[0] + x, ix[1] + y) for (x, y) in instructions_to_coord.values()]

    def tile_update(self, neighborhood_ix: list, v: bool):
        black_neigh = sum([self.state[ix] for ix in neighborhood_ix])
        if (not v and black_neigh == 2) or (
            v and (black_neigh > 2 or black_neigh == 0)
        ):
            return not v
        return v

    def update_state(self):
        self.prepare_reachable_tiles()
        scan_ix = tuple(self.state.keys())
        tmp_layout = self.state.copy()
        for ix in scan_ix:
            tmp_layout[ix] = self.tile_update(
                self.tile_neighborhood(ix), self.state[ix]
            )
        self.state = tmp_layout.copy()


floor_tiling = FloorTiling(tiles)
for d in range(100):
    floor_tiling.update_state()
    print(
        "Black tiles after day {} of flipping: {}".format(
            d, sum(floor_tiling.state.values())
        )
    )
