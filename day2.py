from collections import Counter

# Part 1
good_pass_1 = 0

with open("input/day2.txt") as f:
    line = f.readline()
    while line:
        limits, s, password = line.split()
        s = s[0]
        s_min, s_max = map(int, limits.split("-"))
        s_counter = Counter(password)
        if s_counter[s] >= s_min and s_counter[s] <= s_max:
            good_pass_1 += 1
        line = f.readline()

print("Number of valid passwords (part 1): {}".format(good_pass_1))

# Part 2
good_pass_2 = 0

with open("input/day2.txt") as f:
    line = f.readline()
    while line:
        pos, s, password = line.split()
        s = s[0]
        pos1, pos2 = map(lambda x: int(x) - 1, pos.split("-"))
        if (password[pos1] == s) ^ (password[pos2] == s):
            good_pass_2 += 1
        line = f.readline()

print("Number of valid passwords (part 2): {}".format(good_pass_2))
