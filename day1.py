import numpy as np

ints = np.array([i[:-1] for i in open("input/day1.txt").readlines()], dtype=int)


def find_couple(L, total):
    comp = total - L
    couple = tuple(set(L) & set(comp))
    return couple


# Two numbers
result_2 = find_couple(ints, 2020)

# Three numbers
for tot in 2020 - ints:
    threeple = find_couple(ints, tot)
    if threeple:
        break
result_3 = (2020 - threeple[0] - threeple[1], threeple[0], threeple[1])

print("Couple: {}".format(result_2))
print("Triplet: {}".format(result_3))
