from functools import reduce
from math import gcd

import numpy as np

with open("input/day13.txt") as f:
    arrival_time = int(f.readline()[:-1])
    cycles = np.array(
        [(i, int(j)) for i, j in enumerate(f.readline()[:-1].split(",")) if j != "x"]
    )

bus_IDs = cycles[:, 1]
bus_positions = cycles[:, 0]

# Part 1
waiting_time = bus_IDs - (arrival_time % bus_IDs)
best_bus_ID = np.argmin(waiting_time)
print(
    "Best bus ID * waiting time: {}".format(bus_IDs[best_bus_ID] * np.min(waiting_time))
)

# Part 2
def chinese_remainder(moduli, a):
    S = 0
    N = reduce(lambda x, y: x * y, moduli)
    for n_i, a_i in zip(moduli, a):
        p = N // n_i
        S += a_i * extended_bezout(n_i, p)[-1] * p
    return S, N


def extended_bezout(a: int, b: int):
    """r = gcd(a, b), r = a*u+b*v"""
    r, u, v, rp, up, vp = (a, 1, 0, b, 0, 1)
    while rp != 0:
        q = r // rp
        r, u, v, rp, up, vp = (rp, up, vp, r - q * rp, u - q * up, v - q * vp)
    return (r, u, v)


for i, id1 in enumerate(bus_IDs):
    for _, id2 in enumerate(bus_IDs[i + 1 :]):
        if gcd(id1, id2) != 1:
            print("{} and {} are not pairwise prime !".format(id1, id2))

x, N = chinese_remainder(bus_IDs, -bus_positions)
print("First timestamp to fill schedule condition: {}".format(x % N))
