from collections import Counter
from functools import reduce

groups = []

with open("input/day6.txt") as f:
    tmp_group = []
    for line in f.readlines():
        if line != "\n":
            tmp_group.append(line[:-1])
        else:
            groups.append(tmp_group)
            tmp_group = []
    if tmp_group:
        groups.append(tmp_group)

# Part 1
num_answers = 0
for g in groups:
    num_answers += len(Counter("".join(g)))
print("# of answers: {}".format(num_answers))

# Part 2
common_answers = [
    reduce(lambda x, y: x & y, [set(g) for g in group]) for group in groups
]
print("# of common answers: {}".format(sum(len(s) for s in common_answers)))
