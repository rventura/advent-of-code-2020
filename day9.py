import numpy as np


def has_couple(sum_, preceding):
    comp = sum_ - preceding
    intersection = set(preceding).intersection(comp)
    return len(intersection) > 1


nums = np.array([int(l[:-1]) for l in open("input/day9.txt").readlines()])

# Part 1
window_len = 25
i = 25
while True:
    if not has_couple(nums[i], nums[i - window_len : i]):
        invalid_number = nums[i]
        print("{} doesn't have the required property".format(invalid_number))
        break
    i += 1

# Part 2
for i in range(len(nums)):
    cum = np.cumsum(nums)[i + 1 :] - np.cumsum(nums)[i]
    if invalid_number in cum:
        min_ix = i + 1
        cum_len = (cum - invalid_number).searchsorted(0)
        break

series = nums[min_ix : min_ix + cum_len + 1]
print(
    "The sum of the highest and lowest numbers in the series that sum up to the \
    invalid number is: {}.".format(
        series.min() + series.max()
    )
)
