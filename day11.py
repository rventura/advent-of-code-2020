# coding: utf-8
import numpy as np


def convert_layout(rows, translate_dict):
    n, m = len(rows), len(rows[0])
    int_layout = np.zeros((n, m), dtype=int)
    for i, row in enumerate(rows):
        int_layout[i] = np.array([translate_dict[s] for s in row])
    return int_layout


class Layout:
    def __init__(self, initial_state: np.array, rule: str):
        """Cellular automaton of seats layout"""
        self.state = initial_state.copy()
        if rule == "directions":
            self.rule = self.cell_directions
            self.min_occupied_toggle = 5
        elif rule == "neighborhood":
            self.rule = self.cell_neighborhood
            self.min_occupied_toggle = 4

    def get_cell_update(self, cell_neighborhood: np.array, v: int):
        if v == 0 and not (cell_neighborhood == 1).any():
            return 1
        if v == 1 and (cell_neighborhood == 1).sum() >= self.min_occupied_toggle:
            return 0
        return v

    def cell_neighborhood(self, i, j):
        neigh_ix = ([i - 1, i, i + 1], [j - 1, j, j + 1])
        for dim, size in enumerate(self.state.shape):
            if -1 in neigh_ix[dim]:
                neigh_ix[dim].remove(-1)
            if size in neigh_ix[dim]:
                neigh_ix[dim].remove(size)
        product = [[ii, jj] for ii in neigh_ix[0] for jj in neigh_ix[1]]
        product.remove([i, j])
        return np.array([self.state[ix] for ix in product])

    def cell_directions(self, i, j):
        directions_dict = {
            "NW": (-1, -1),
            "N": (-1, 0),
            "NE": (-1, 1),
            "W": (0, -1),
            "E": (0, 1),
            "SW": (1, -1),
            "S": (1, 0),
            "SE": (1, 1),
        }
        seats_in_view = {}
        for direction, (inc_row, inc_col) in directions_dict.items():
            position = [i, j]
            while True:
                position[0] += inc_row
                position[1] += inc_col
                if (np.array(position) < 0).any() or (
                    np.array(position) >= self.state.shape
                ).any():
                    seats_in_view[direction] = 2
                    break
                if self.state[tuple(position)] != 2:
                    seats_in_view[direction] = self.state[tuple(position)]
                    break
        return np.array(list(seats_in_view.values()))

    def update_state(self):
        tmp_layout = np.zeros_like(self.state)
        for (i, j), v in np.ndenumerate(self.state):
            considered_seats = self.rule(i, j)
            tmp_layout[i, j] = self.get_cell_update(considered_seats, v)
        self.state = tmp_layout.copy()


lines = [l[:-1] for l in open("input/day11.txt").readlines()]
seats_int = {"L": 0, "#": 1, ".": 2}
int_seats = {v: k for k, v in seats_int.items()}
initial_layout = convert_layout(lines, seats_int)

# Part 1
all_neigh_layouts = []
neigh_layout = Layout(initial_layout, rule="neighborhood")
stable_state = False
while not stable_state:
    all_neigh_layouts.append(neigh_layout.state)
    neigh_layout.update_state()
    if (all_neigh_layouts[-1] == neigh_layout.state).all():
        stable_state = True
print(
    "Number of occupied seats (neighborhood rule): {}".format(
        (neigh_layout.state == 1).sum()
    )
)

# Part 2
all_directions_layouts = []
directions_layout = Layout(initial_layout, rule="directions")
stable_state = False
while not stable_state:
    all_directions_layouts.append(directions_layout.state)
    directions_layout.update_state()
    if (all_directions_layouts[-1] == directions_layout.state).all():
        stable_state = True
print(
    "Number of occupied seats (directions rule): {}".format(
        (directions_layout.state == 1).sum()
    )
)
