from __future__ import annotations

from collections import defaultdict
from functools import reduce
from itertools import combinations
from operator import mul

import numpy as np


class Tile:
    def __init__(self, data):
        """Class storing tiles geometry, allowing for rotation."""

        self.data = np.array([list(l) for l in data])
        self.actions = []

    def rotate(self, n=1, log=True):
        """Rotate tile 90° counterclockwise."""

        for _ in range(n):
            self.data = self.data.T[::-1]
        if log:
            self.actions.extend(list(n * "r"))

    def flip(self, axis="h", log=True):
        """Flip tile along given axis."""

        if axis == "h":
            self.data = self.data[::-1]
        elif axis == "v":
            self.rotate(2)
            self.flip("h")
        if log:
            self.actions.append(axis)

    def reset(self):
        while self.actions:
            last_action = self.actions.pop()
            if last_action in ["v", "h"]:
                self.flip(last_action, False)
            else:
                self.rotate(3, False)

    def describe_edges(self):
        edges = {
            "N": self.data[0],
            "S": self.data[-1],
            "E": self.data[:, -1],
            "W": self.data[:, 0],
        }
        edges.update({k + "_rev": v[::-1] for k, v in edges.items()})
        return edges

    def get_matches(self, tile: Tile) -> tuple:
        """Return side names that match between this tile and another tile.

        If there is no match, return the empty tuple. Make it so that this
        tile side is never reversed.
        """

        def reverse(match):
            return (match[0][0], match[1] + "_rev")

        for other_pos, other_edge in tile.describe_edges().items():
            for pos, edge in self.describe_edges().items():
                if (other_edge == edge).all():
                    match = (pos, other_pos)
                    return match if match[0].find("rev") == -1 else reverse(match)
        return ()

    def set_position(self, initial, target):
        """Set tile position according to a target orientation."""

        orientation_cycle = dict(
            [
                ("N", "W_rev"),
                ("W_rev", "S_rev"),
                ("S_rev", "E"),
                ("E", "N"),
                ("N_rev", "W"),
                ("W", "S"),
                ("S", "E_rev"),
                ("E_rev", "N_rev"),
            ]
        )
        current_orientation = initial
        while current_orientation[0] != target:
            current_orientation = orientation_cycle[current_orientation]
            self.rotate()
        if current_orientation[-3:] == "rev":
            if current_orientation[0] in ["N", "S"]:
                self.flip("v")
            else:
                self.flip("h")


lines = [l[:-1] for l in open("input/day20.txt").readlines()]

# Read input
tiles = {}
i = 0
while True:
    seq = lines[(i * 12) : (i * 12) + 1 + 10]
    if seq:
        tiles[int(seq[0].split()[1][:-1])] = Tile(seq[1:])
        i += 1
    else:
        break

# Part 1
all_associations = defaultdict(list)
for id0, id1 in combinations(tiles, 2):
    association = tiles[id0].get_matches(tiles[id1])
    if association:
        all_associations[id0].append(id1)
        all_associations[id1].append(id0)

corners_ids = [id_ for id_, m in all_associations.items() if len(m) == 2]
print("Product of corner tiles ids: {}".format(reduce(mul, corners_ids)))

# Part 2
def connect_with_preceding_tile(
    preceding_id: int, associations: list, tiles: dict, orientation: tuple
) -> int:
    """Gets the next tile id, and sets it to the correct orientation.

    Placing gets done from upper left to lower right."""
    # print("id {}: {} associations".format(preceding_id, associations[preceding_id]))
    for id_ in associations[preceding_id]:
        candidate_tile = tiles[id_]
        sides = tiles[preceding_id].get_matches(candidate_tile)
        if sides[0] == orientation[0]:
            start = sides[1]
            tiles[id_].set_position(start, orientation[1])
            return id_
    raise ValueError("At least one tile should fit for tile {}!".format(preceding_id))


def line_to_bool(s):
    return [char == "#" for char in s]


def scan_for_monster(data, mask):
    n, m = data.shape
    k, l = mask.shape
    monsters = 0
    for i in range(n - k):
        for j in range(m - l):
            if (data[i : i + k, j : j + l][mask] == "#").all():
                monsters += 1
    return monsters


# Set tiles to get the whole map, beginning with the upper left one.
tile_map = np.zeros((12, 12), dtype=int)
image = np.zeros(((12 * 8, 12 * 8)), dtype=str)
tiles[3067].rotate()  # After one rotation, 3067 is suitable to be the upper left corner
ix_to_id = {(0, 0): 3067}

for ix in np.ndindex(*tile_map.shape):
    if ix[1] != 0:
        next_id = connect_with_preceding_tile(
            ix_to_id[(ix[0], ix[1] - 1)], all_associations, tiles, ("E", "W")
        )
        ix_to_id[ix] = next_id
    elif ix[1] == 0 and ix[0] != 0:
        next_id = connect_with_preceding_tile(
            ix_to_id[(ix[0] - 1, ix[1])], all_associations, tiles, ("S", "N")
        )
        ix_to_id[ix] = next_id

for ix, id_ in ix_to_id.items():
    trimmed_tile = tiles[id_].data[1:-1, 1:-1]
    st_ix = 8 * np.array(ix)
    image[st_ix[0] : st_ix[0] + 8, st_ix[1] : st_ix[1] + 8] = trimmed_tile

image_as_tile = Tile(image)
sea_monster_mask = np.array(
    [
        line_to_bool("                  # "),
        line_to_bool("#    ##    ##    ###"),
        line_to_bool(" #  #  #  #  #  #   "),
    ],
    dtype=bool,
)

roughness = 0
while not roughness:
    for rot in range(3):
        n_monsters = scan_for_monster(image_as_tile.data, sea_monster_mask)
        if not n_monsters:
            image_as_tile.rotate()
        else:
            roughness = (
                image_as_tile.data == "#"
            ).sum() - n_monsters * sea_monster_mask.sum()
            break
    image_as_tile.flip()
