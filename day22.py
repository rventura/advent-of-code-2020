from collections import deque


def read_decks():
    d1 = []
    d2 = []
    with open("input/day22.txt") as f:
        part1, part2 = f.read().split("\n\n")
        d1 = [int(i) for i in part1.split("\n")[1:] if i]
        d2 = [int(i) for i in part2.split("\n")[1:] if i]
    return d1, d2


# Part 1
p1_deck, p2_deck = read_decks()
while p1_deck and p2_deck:
    p1_card = p1_deck.pop(0)
    p2_card = p2_deck.pop(0)
    if p1_card > p2_card:
        p1_deck.extend([p1_card, p2_card])
    else:
        p2_deck.extend([p2_card, p1_card])

if p1_deck:
    score = sum(i * s for i, s in enumerate(p1_deck[::-1], start=1))
    print("Player 1 wins with score: {}".format(score))
if p2_deck:
    score = sum(i * s for i, s in enumerate(p2_deck[::-1], start=1))
    print("Player 2 wins with score: {}".format(score))

# Part 2
class RecursiveCombatGame:
    """Holds information for a recursive combat game, allowing to recurse and print
    what is going on."""

    def __init__(self, deck1, deck2, game_num=1, verbose=True):
        "Initiate game with players decks"
        self.deck1 = deque(deck1)
        self.deck2 = deque(deck2)
        self.game_history = set()
        self.game_num = game_num
        self.played_games = [game_num]
        self.round_num = 1
        self.game_winner = None
        self.verbose = verbose

    def play(self):
        while self.deck1 and self.deck2:
            if self.already_played():
                break
            if self.verbose:
                print(
                    "=== Game {} ==="
                    "\n"
                    "-- Round {} (Game {}) --"
                    "\n"
                    "Player 1's deck: {}\n"
                    "Player 2's deck: {}\n"
                    "Player 1 plays: {}\n"
                    "Player 2 plays: {}\n".format(
                        self.game_num,
                        self.round_num,
                        self.game_num,
                        self.deck1,
                        self.deck2,
                        self.deck1[0],
                        self.deck2[0],
                    )
                )
            card1, card2 = self.deck1.popleft(), self.deck2.popleft()
            if card1 > len(self.deck1) or card2 > len(self.deck2):
                # Play simple Combat
                self.round_winner = 1 if card1 > card2 else 2
            else:
                # Recurse Combat
                recursive_game = RecursiveCombatGame(
                    list(self.deck1)[:card1],
                    list(self.deck2)[:card2],
                    game_num=max(self.played_games) + 1,
                    verbose=self.verbose,
                )
                recursive_game.play()
                self.round_winner = recursive_game.game_winner
                self.played_games.extend(recursive_game.played_games)
                if self.verbose:
                    print(
                        "Playing a sub-game to determine the winner..."
                        "\n\n"
                        "...anyway, back to game {}\n".format(self.game_num)
                    )
            # End round
            if self.verbose:
                print(
                    "Player {} wins round {} of game {}!\n".format(
                        self.round_winner, self.round_num, self.game_num
                    )
                )
            if self.round_winner == 1:
                self.deck1.extend([card1, card2])
            else:
                self.deck2.extend([card2, card1])
            self.round_num += 1
        # End game
        if self.game_winner is None:
            self.game_winner = 1 if self.deck1 else 2
        if self.verbose:
            print(
                "The winner of game {} is player {}!\n".format(
                    self.game_num,
                    self.game_winner,
                )
            )

    def already_played(self):
        state = (tuple(self.deck1), tuple(self.deck2))
        if state in self.game_history:
            if self.verbose:
                print(
                    "Already played this configuration:"
                    "\n"
                    "Player 1's deck: {}\n"
                    "Player 2's deck: {}\n".format(
                        self.deck1,
                        self.deck2,
                    )
                )
            self.game_winner = 1
            return True
        self.game_history.add(state)
        return False

    def compute_score(self):
        if self.game_winner is not None:
            winner_deck = self.deck1 if self.game_winner == 1 else self.deck2
            return sum(i * s for i, s in enumerate(list(winner_deck)[::-1], start=1))
        raise ValueError("Game has not been played yet!")


# Play game !
rcg = RecursiveCombatGame(*read_decks(), verbose=False)
rcg.play()
final_score = rcg.compute_score()
print("Final winner score is {}".format(final_score))
