from functools import reduce


def parse_line(line):
    ing_sep = line.find(" (")
    ingredient_list = line[:ing_sep].split()
    all_sep = line.find("contains ")
    allergen_list = line[all_sep + len("contains ") : -1].split(", ")
    return ingredient_list, allergen_list


couples = [parse_line(l[:-1]) for l in open("input/day21.txt").readlines()]
food_ingredients, food_allergens = zip(*couples)
all_allergens = reduce(set.union, [set(al) for al in food_allergens])
all_ingredients = reduce(set.union, [set(ing) for ing in food_ingredients])

# Part 1
not_associated = {}
for allergen in all_allergens:
    candidate_ingredients = [set(c[0]) for c in couples if allergen in c[1]]
    not_associated[allergen] = all_ingredients - reduce(
        set.intersection, candidate_ingredients
    )
discardable = reduce(set.intersection, not_associated.values())

discardable_appearances = 0
for ingredients in food_ingredients:
    discardable_appearances += len(set(ingredients) & discardable)

print(
    "There are {} appearances of ingredients that can be discarded.".format(
        discardable_appearances
    )
)

# Part 2
dangerous_ingredients = all_ingredients - discardable
association = {}
while len(association) < len(dangerous_ingredients):
    already_associated = set(association.values())
    for allergen, excl in not_associated.items():
        candidate_set = all_ingredients - excl - already_associated
        if len(candidate_set) == 1:
            association[allergen] = candidate_set.pop()

print(
    "Canonical dangerous list: {}".format(
        ",".join(association[k] for k in sorted(association))
    )
)
