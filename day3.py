import functools

import numpy as np


def conv(s):
    return 0 if s == "." else 1


lines = [l[:-1] for l in open("input/day3.txt").readlines()]
template_forest = np.zeros((len(lines), len(lines[0])), dtype=int)
for i, line in enumerate(lines):
    for j, s in enumerate(line):
        template_forest[i, j] = conv(s)


def count_trees(coord_right, coord_down):
    min_slope_width = (template_forest.shape[0] // coord_down) * coord_right
    coeff = int(min_slope_width / template_forest.shape[1]) + 1
    complete_forest = np.tile(template_forest, coeff)

    tree_counter = 0
    i, j = (0, 0)
    while i < complete_forest.shape[0]:
        tree_counter += complete_forest[i, j]
        i += coord_down
        j += coord_right
    return tree_counter


tot_trees = {}
for (cr, cd) in [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]:
    tot_trees[(cr, cd)] = count_trees(cr, cd)

result = functools.reduce(lambda x, y: x * y, tot_trees.values())
