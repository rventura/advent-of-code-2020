from itertools import product

import numpy as np


def convert_layout(rows, translate_dict, n_turns, cube_dim):
    size = len(rows) + 2 * n_turns
    grid = np.zeros(tuple(size for _ in range(cube_dim)), dtype=bool)
    for i, row in enumerate(rows):
        bool_row = np.array([translate_dict[s] for s in row])
        if cube_dim == 3:
            grid[size // 2, i + n_turns, n_turns : n_turns + len(bool_row)] = bool_row
        if cube_dim == 4:
            grid[
                size // 2, size // 2, i + n_turns, n_turns : n_turns + len(bool_row)
            ] = bool_row
    return grid


class CubeLayout:
    def __init__(self, state: np.array):
        """Cellular automaton of Conway cubes"""
        self.state = state.copy()
        self.dim = state.ndim

    def get_cell_update(self, cell_neighborhood: np.array, v: bool):
        if not v and cell_neighborhood.sum() == 3:
            return True
        if v and not 2 <= cell_neighborhood.sum() <= 3:
            return False
        return v

    def cell_neighborhood(self, cell_ix: tuple):
        neigh_ix = [[ix - 1, ix, ix + 1] for ix in cell_ix]
        for dim, size in enumerate(self.state.shape):
            if -1 in neigh_ix[dim]:
                neigh_ix[dim].remove(-1)
            if size in neigh_ix[dim]:
                neigh_ix[dim].remove(size)
        neigh = list(product(*neigh_ix))
        neigh.remove(cell_ix)
        return np.array([self.state[ix] for ix in neigh])

    def update_state(self):
        tmp_layout = np.zeros_like(self.state)
        for index, v in np.ndenumerate(self.state):
            cube_neighborhood = self.cell_neighborhood(index)
            tmp_layout[index] = self.get_cell_update(cube_neighborhood, v)
        self.state = tmp_layout.copy()


lines = [l[:-1] for l in open("input/day17.txt").readlines()]
status_int = {".": False, "#": True}
int_seats = {v: k for k, v in status_int.items()}
turns = 6

# Part 1
cubes3d = CubeLayout(convert_layout(lines, status_int, turns, cube_dim=3))
for _ in range(turns):
    cubes3d.update_state()
print("Active 3d-cubes after {} turns : {}".format(turns, cubes3d.state.sum()))

# Part 2
cubes4d = CubeLayout(convert_layout(lines, status_int, turns, cube_dim=4))
for _ in range(turns):
    cubes4d.update_state()
print("Active 4d-cubes after {} turns : {}".format(turns, cubes4d.state.sum()))
