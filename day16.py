from functools import reduce

import pandas as pd


def parse_field(s):
    name, intervals = s.split(": ")
    intervals = [
        range(int(r.split("-")[0]), int(r.split("-")[1]) + 1)
        for r in intervals.split(" or ")
    ]
    return name, intervals


# Part 1
with open("input/day16.txt") as f:
    fields_intervals = {}
    for _ in range(20):
        fields_intervals.setdefault(*parse_field(f.readline()[:-1]))
all_tickets = pd.read_csv(
    "input/day16.txt", skiprows=list(range(22)) + list(range(23, 25)), header=None
)
my_ticket = all_tickets.loc[0, :]
nearby_tickets = all_tickets.loc[1:, :]

overall_validity_set = reduce(
    lambda x, y: set(x) | set(y),
    [set(r[0]) | set(r[1]) for r in fields_intervals.values()],
)

m, M = min(overall_validity_set), max(overall_validity_set)
if M - m + 1 == len(overall_validity_set):
    under_min = (nearby_tickets - m) < 0
    over_max = (nearby_tickets - M) > 0
    error_rate = (nearby_tickets[under_min | over_max].sum()).sum()
print("Error rate: {}".format(error_rate))

# Part 2
valid_tickets = nearby_tickets[~(under_min | over_max).any(axis=1)]
num_cols = valid_tickets.shape[1]
col_names = num_cols * [""]
already_set_fields = []
while not all(col_names):
    not_set = set(range(num_cols)) - set(already_set_fields)
    for field in fields_intervals:
        lower, upper = tuple(fields_intervals[field])
        is_inside_range = (
            valid_tickets.loc[:, not_set].isin(lower)
            | valid_tickets.loc[:, not_set].isin(upper)
        ).all(axis=0)
        if sum(is_inside_range) == 1:
            set_ix = is_inside_range.idxmax()
            col_names[set_ix] = field
            already_set_fields.append(set_ix)
            break

valid_tickets.columns = col_names
my_ticket.index = col_names
departure_cols = [c for c in valid_tickets.columns if c.find("departure") > -1]
res = reduce(lambda x, y: x * y, my_ticket.loc[departure_cols])
print("Departure fields multiplied together: {}".format(res))
