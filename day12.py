import numpy as np


class Ship:
    def __init__(
        self,
        position_inc: dict,
        orientation_change: dict,
        initial_orientation="E",
        initial_position=(0, 0),
    ):
        self.position = np.array(initial_position)
        self.orientation = initial_orientation
        self.position_inc = position_inc
        self.orientation_change = orientation_change

    def follow_instruction(self, instruction):
        orientation, num = instruction
        if orientation in ["L", "R"]:
            self.orientation = self.orientation_change[orientation][
                (self.orientation, num)
            ]
        elif orientation == "F":
            self.position += num * self.position_inc[self.orientation]
        else:
            self.position += num * self.position_inc[orientation]


class WaypointShip:
    def __init__(self, w_rules, initial_waypoint=(10, 1), initial_position=(0, 0)):
        self.position = np.array(initial_position)
        self.waypoint = np.array(initial_waypoint)
        self.waypoint_rules = w_rules

    def follow_instruction(self, instruction):
        if instruction[0] == "F":
            self.move_ship(instruction[1])
        else:
            self.move_waypoint(*instruction)

    def move_waypoint(self, orientation, num):
        if orientation in ["L", "R"]:
            ship_to_waypoint = self.waypoint - self.position
            self.waypoint = self.position + self.waypoint_rules[orientation][num](
                *ship_to_waypoint
            )
        else:
            self.waypoint += num * self.waypoint_rules[orientation]

    def move_ship(self, num):
        ship_to_waypoint = self.waypoint - self.position
        self.position += num * (self.waypoint - self.position)
        self.waypoint = self.position + ship_to_waypoint


lines = [l[:-1] for l in open("input/day12.txt").readlines()]
instructions_list = [(ins[0], int(ins[1:])) for ins in lines]
position_increments = {
    "N": np.array((0, 1)),
    "W": np.array((-1, 0)),
    "E": np.array((1, 0)),
    "S": np.array((0, -1)),
}
orientation_shift = {
    "L": {
        ("N", 90): "W",
        ("W", 90): "S",
        ("E", 90): "N",
        ("S", 90): "E",
        ("N", 180): "S",
        ("W", 180): "E",
        ("E", 180): "W",
        ("S", 180): "N",
        ("N", 270): "E",
        ("W", 270): "N",
        ("E", 270): "S",
        ("S", 270): "W",
    },
    "R": {
        ("N", 90): "E",
        ("W", 90): "N",
        ("E", 90): "S",
        ("S", 90): "W",
        ("N", 180): "S",
        ("W", 180): "E",
        ("E", 180): "W",
        ("S", 180): "N",
        ("N", 270): "W",
        ("W", 270): "S",
        ("E", 270): "N",
        ("S", 270): "E",
    },
}
waypoint_rules = {
    "L": {
        90: lambda x, y: np.array((-y, x)),
        180: lambda x, y: np.array((-x, -y)),
        270: lambda x, y: np.array((y, -x)),
    },
    "R": {
        90: lambda x, y: np.array((y, -x)),
        180: lambda x, y: np.array((-x, -y)),
        270: lambda x, y: np.array((-y, x)),
    },
}
waypoint_rules.update(position_increments)

# Part1
ship = Ship(position_increments, orientation_shift)
for ins in instructions_list:
    ship.follow_instruction(ins)
print(
    "Manhattan distance from start (first ship): {}".format(np.abs(ship.position).sum())
)

# Part 2
ship2 = WaypointShip(waypoint_rules)
for ins in instructions_list:
    ship2.follow_instruction(ins)
print(
    "Manhattan distance from start (second ship): {}".format(
        np.abs(ship2.position).sum()
    )
)
