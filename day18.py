from operator import add, mul
from string import digits
from typing import Callable, Union


def find_matching_parentheses(expr: Union[list, str]) -> list[tuple]:
    couples = []
    opening_ix = []
    for i, char in enumerate(expr):
        if char == "(":
            opening_ix.append(i)
        elif char == ")":
            couples.append((opening_ix.pop(), i))
    return couples


def surround_plus_signs(expr: str) -> str:
    def insert_par(expr_lst: list, plus_pos: int, par_couples: list[tuple]) -> list:
        left_ix, right_ix = plus_pos - 1, plus_pos + 1
        if expr_lst[left_ix] in digits:
            expr_lst.insert(left_ix, "(")
        elif expr_lst[left_ix] == ")":
            opening_par_ix = [x for x in par_couples if x[1] == left_ix].pop()[0]
            expr_lst.insert(opening_par_ix, "(")
        if expr_lst[right_ix + 1] in digits:
            expr_lst.insert(right_ix + 2, ")")
        elif expr_lst[right_ix + 1] == "(":
            closing_par_ix = [x for x in par_couples if x[0] == right_ix].pop()[1]
            expr_lst.insert(closing_par_ix + 1, ")")
        return expr_lst

    expr = list(expr.replace(" ", ""))
    num_plus = expr.count("+")
    plus_ix = 0
    for _ in range(num_plus):
        plus_ix = expr.index("+", plus_ix + 1)
        par_ix = find_matching_parentheses(expr)
        expr = insert_par(expr, plus_ix, par_ix)
        plus_ix += 1
    return " ".join(expr)


def parse_expression(expr_lst: list, op: Callable = add, acc: int = 0) -> int:
    if not expr_lst:
        return acc
    s = expr_lst.pop()
    if s in ["+", "*"]:
        op = add if s == "+" else mul
    elif s in digits:
        acc = op(acc, int(s))
    elif s == "(":
        acc = op(acc, parse_expression(expr_lst, add, 0))
    elif s == ")":
        return acc
    elif s == " ":
        pass
    else:
        raise ValueError("Invalid value {}".format(s))
    return parse_expression(expr_lst, op, acc)


# Part 1
expressions = [l[:-1] for l in open("input/day18.txt").readlines()]
results = [parse_expression(list(x)[::-1]) for x in expressions]
print("Sum of expressions results: {}".format(sum(results)))

# Part 2
results_2 = [parse_expression(list(surround_plus_signs(x))[::-1]) for x in expressions]
print(
    "Sum of expressions results, with higher precedence for addition: {}".format(
        sum(results_2)
    )
)
